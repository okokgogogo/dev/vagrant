#!/usr/bin/env bash
git submodule init
git submodule add https://gitlab.com/okokgogogo/dev/jvm-development-environment.git
git submodule add https://github.com/kurron/ansible-pull-transparent.git ansible-pull-jvm-development
git submodule update
