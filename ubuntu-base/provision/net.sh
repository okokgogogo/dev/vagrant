
sudo echo "network:
  version: 2
  ethernets:
  eth0:
  dhcp4: true
  auto eth1
  iface eth2 inet static
  address 192.168.99.239
  netmask 255.255.255.0
  gateway 192.168.99.100" > /etc/network/interfaces

